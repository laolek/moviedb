using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Repositories.Contracts;

namespace Repositories
{
    public class MovieRepository : IBaseRepository<Movie>
    {
        private ICollection<Movie> Movies { get; set; } 
        
        public MovieRepository()
        {
            Movies = new Movie[]
            {
                // ids would be added automatically if added to a real (or in-memory) DB
                // could create a similar autoincrement method for it instead of hardcoding
                new Movie{Id = 1, CategoryId = 1, Description = "not released yet, description TBA", 
                    Year = 2020, Title = "Something creative here", Rating = 5},
                new Movie{Id = 2, CategoryId = 2, Description = "not released yet, description TBA", 
                    Year = 2021, Title = "New blockbuster"},
                new Movie{Id = 3, CategoryId = 1, Description = "not released yet but it's going to be good, description TBA", 
                    Year = 2022, Title = "New Oscars candidate", Rating = 1},
            };

        }

        public ICollection<Movie> FindAll()
        {
            return Movies;
        }
        
        public Movie FindById(int id)
        {
            try
            {
                return Movies.First(m => m.Id == id);
            }
            catch (InvalidOperationException e)
            {
                return null;
            }
            
        }
    }
}