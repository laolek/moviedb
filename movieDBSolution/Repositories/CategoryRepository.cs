using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Repositories.Contracts;

namespace Repositories
{
    // the repository wasn't requested in the task but necessary to eventually display the Ids as Names
    public class CategoryRepository : IBaseRepository<Category>
    {
        private ICollection<Category> Categories { get; set; }
        
        public CategoryRepository()
        {
            Categories = new Category[]
            {
                new Category {Name = "sci-fi", Id = 1},
                new Category {Name = "drama", Id = 2},
                new Category {Name = "documentary", Id = 3}
            };
        } 
        

        public ICollection<Category> FindAll()
        {
            return Categories;
        }

        public String FindNameById(int? id)
        {
            try
            {
                return Categories.First(c => c.Id == id).Name;    
            }
            catch (InvalidOperationException e)
            {
                return null;
            }
        }
    }
}