﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using DTO;
using Repositories;

namespace Services
{
    public class MovieService
    {
        private readonly MovieRepository _movieRepository;
        private CategoryRepository _categoryRepository;

        public MovieService(MovieRepository movieRepository, CategoryRepository categoryRepository)
        {
            _movieRepository = movieRepository;
            _categoryRepository = categoryRepository;
        }

        public ICollection<MovieOverview> GetAllMovies()
        {
            return _movieRepository.FindAll().Select(m => new MovieOverview()
            {
                Id = m.Id,
                Title = m.Title,
                CategoryName = _categoryRepository.FindNameById(m.CategoryId),
                Rating = m.Rating,
                Year = m.Year
            }).ToList();
        }

        public MovieDetails GetMovieById(int id)
        {
            var movie = _movieRepository.FindById(id);
            if (movie == null)
            {
                return null;
            }

            var newMovie = new MovieDetails()
            {
                Id = movie.Id,
                Description = movie.Description,
                Title = movie.Title,
                Year = movie.Year,
                Rating = movie.Rating
            };
            return newMovie;
        }
    }
}