using System;

namespace DTO
{
    public class MovieDetails
    {
        
        public int Id { get; set; }
        public String Title { get; set; }
        
        public int Year { get; set; }
        
        public String Description { get; set; }
        public int? Rating { get; set; }
    }
}