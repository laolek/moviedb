﻿using System;

namespace DTO
{
    public class MovieOverview
    {
        public int Id { get; set; }
        public String Title { get; set; }
        
        public int Year { get; set; }
        
        public int? Rating { get; set; }
        
        public String CategoryName { get; set; }
    }
}