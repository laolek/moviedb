using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Category : BaseEntity
    {
        [MinLength(1)]
        [MaxLength(200)]
        [Required]
        public String Name { get; set; }
    }
}