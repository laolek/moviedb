﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Movie : BaseEntity
    {
        
        [MinLength(1)]
        [MaxLength(200)]
        [Required]
        public String Title { get; set; }
        
        public int Year { get; set; }
        
        [MaxLength(2000)]
        public String Description { get; set; }
        public int? Rating{ get; set; }
        
        public int? CategoryId { get; set; }
    }
}