using System.Collections.Generic;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : Controller
    {
        private MovieService _movieService;

        public MovieController(MovieService movieService)
        {
            _movieService = movieService;
        }
        
        [HttpGet]
        public ICollection<MovieOverview> GetMovies()
        {
            return _movieService.GetAllMovies();
        }
        
        [HttpGet("{id}")]
        public ActionResult<MovieDetails> GetMovieById(int id)
        {
            var movie = _movieService.GetMovieById(id);
            if (movie == null)
            {
                return NotFound();
            }
            return _movieService.GetMovieById(id);
        }
    }
}