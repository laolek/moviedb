﻿using System.Collections.Generic;

namespace Repositories.Contracts
{
    public interface IBaseRepository<T>
    {
        ICollection<T> FindAll();
    }
}