import axios from 'axios'
const baseUrl = 'https://localhost:5001/api/movie/'

async function getAllMovies () {
  let response = await axios.get(baseUrl)
  return response
}

async function getMovie (id) {
  let response = await axios.get(baseUrl + id)
  return response
}

export default {
  getAllMovies () {
    return getAllMovies()
  },
  getMovieById (id) {
    return getMovie(id)
  }
}
